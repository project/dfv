This modules enables site admins and developers to make the allowed values of a field depend on another proviously selected field's value.

You can use views with arguments to let views return the allowed values for a dependent field.